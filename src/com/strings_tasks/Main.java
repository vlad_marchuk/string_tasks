package com.strings_tasks;

import com.strings_tasks.services.Conversion;
import com.strings_tasks.services.OneString;
import com.strings_tasks.services.WorkWithStrings;

import java.util.Scanner;

public class Main {


    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
//1. Вывести в одну строку символы:

        OneString service = new OneString();
        service.getAsciiAtoZ('A', 'Z');
        System.out.println();
        service.getAsciiZtoA('z', 'a');
        System.out.println();
        service.getAsciiAtoZ('а', 'я');
        System.out.println();
        service.getAsciiAtoZ('0', '9');
        System.out.println();
        service.getAsciiAtoZ(' ', '~');


//        2. Написать и протестировать функции преобразования:

        Conversion conversion = new Conversion();
        System.out.println(conversion.convertIntToString(45));
        System.out.println(conversion.convertDoubleToString(48.254));
        System.out.println(conversion.convertStringToInt("458"));
        System.out.println(conversion.convertStringToDouble("125.325"));



//        3. Написать и протестировать функции работы со строками:

        WorkWithStrings workWithStrings = new WorkWithStrings(scanner);

        //Task 1
      System.out.println("Длинна самого короткого слова -  "+workWithStrings.getLenghtMinWord("Трактористы ехали на ранчо, но забыли трактора и копали лопатами?!"));
        //Task 2
        String[] wordsArray = new String[]{"Береза", "тракторист", "Чехия", "пиво", "дуб", "столешница", "монитор"};
        workWithStrings.replaceLastThreeLettersDollars(wordsArray,5);
        //Task 3
       System.out.println(workWithStrings.addSpacesAfterPunctuationMarks("Трактористы,ехали на. ранчо, но забыли. трактора,и копали;лопатами"));
       //Task 4
       System.out.println(workWithStrings.leaveOneCharacterInTheString("Погода,хорошая,очень!"));
       //Task 5
       System.out.println(workWithStrings.countWordsInTheLine());
        //Task 6
       System.out.println(workWithStrings.removePartFromString("Молоко,ехали на. ранчо, но забыли. трактора,и копали;лопатами",1,5));
        //Task 7
        System.out.println(workWithStrings.flipTheLine("Молоко,ехали на. ранчо, но забыли. трактора,и копали;лопатами"));
       //Task 8
        System.out.println(workWithStrings.deleteLastWord("Молоко,ехали на. ранчо, но забыли. трактора,и копали;лопатами"));


}

}


