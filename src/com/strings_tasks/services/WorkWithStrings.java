package com.strings_tasks.services;

import java.util.Scanner;

public class WorkWithStrings {

    private final Scanner scanner;

    public WorkWithStrings(Scanner scanner) {
        this.scanner = scanner;
    }


    public int getLenghtMinWord(String str) {
        String myArray[] = str.split(" |,|\\.|!|\\?|:");
        int length;
        int lenghtMinWord = 500;
        for (int i = 0; i < myArray.length; i++) {
            length = myArray[i].length();
            if (length != 0 && length < lenghtMinWord) {
                lenghtMinWord = length;
            }
        }
        return lenghtMinWord;
    }


    public void replaceLastThreeLettersDollars(String[] wordsArray, int lengthSetWord) {
        String firstHalfWord;
        String secondHalfWord = "$$$";
        int checker = 0;


        if (lengthSetWord < 3) {
            System.out.println("Длинна слова для операции должна быть больше или ровна 3");
            System.exit(0);
        }

        for (int i = 0; i < wordsArray.length; i++) {
            if (wordsArray[i].length() == lengthSetWord) {
                firstHalfWord = wordsArray[i].substring(0, lengthSetWord - 3);
                String updateWord = firstHalfWord.concat(secondHalfWord);
                System.out.println(updateWord);
                checker++;
            }
        }
        if (checker == 0) {
            System.out.println("Слова с такой длинной нету в списке слов!");
        }

    }

    public String addSpacesAfterPunctuationMarks(String updateString) {

        updateString = updateString.replaceAll("\\. ", ".");
        updateString = updateString.replaceAll(", ", ",");
        updateString = updateString.replaceAll("; ", ";");
        updateString = updateString.replaceAll(": ", ":");
        updateString = updateString.replaceAll("! ", "!");
        updateString = updateString.replaceAll("\\? ", "?");

        updateString = updateString.replaceAll("\\.", ". ");
        updateString = updateString.replaceAll(",", ", ");
        updateString = updateString.replaceAll(";", "; ");
        updateString = updateString.replaceAll(":", ": ");
        updateString = updateString.replaceAll("!", "! ");
        updateString = updateString.replaceAll("\\?", "? ");

        return updateString;
    }

    public String leaveOneCharacterInTheString(String sentence) {
        char[] strArr = sentence.toCharArray();
        for (int i = 0; i < strArr.length; i++) {
            for (int j = i + 1; j < strArr.length; j++) {
                if (strArr[i] == strArr[j]) {
                    strArr[j] = ' ';
                }
            }
        }
        sentence = new String(strArr);
        return sentence;
    }

    public int countWordsInTheLine() {
        System.out.println("Введите строку");
        String str = scanner.nextLine();
        int count = 0;
        String arrWord[] = str.split(" |,|\\.|!|\\?|:");
        for (int i = 0; i < arrWord.length; i++) {
            if (arrWord[i].length() > 0) {
                count++;

            }
        }
        return count;
    }

    public String removePartFromString(String str, int startCut, int lengthCut) {
        if (startCut >= 0 && lengthCut <= (str.length() - startCut) && lengthCut >= 0) {
            String cutFirstHalf = str.substring(0, startCut);
            String cutSecondHalf = str.substring(startCut + lengthCut, str.length());
            String cutString = cutFirstHalf.concat(cutSecondHalf);
            return cutString;
        } else
            return ("Указанные параметры обрезания строки выходят за границу даной строки ");
    }

    public String flipTheLine(String str) {
        char[] strArr = str.toCharArray();
        String reverseStr = "";
        int b = (strArr.length - 1);
        char[] reversMyArray = new char[strArr.length];
        for (int i = 0; i < strArr.length; i++) {
            reversMyArray[i] = strArr[b];
            b--;
        }
        for (char z : reversMyArray) {
            reverseStr += z;
        }
        return reverseStr;
    }

    public String deleteLastWord(String str) {
        int lengthLastElement;
        String[] wordsArr = str.split(" |,|\\.|:|;|!|\\?|\\(|\\)");
        lengthLastElement = wordsArr[wordsArr.length - 1].length();
        String lastWorld = wordsArr[wordsArr.length - 1];
        int lastWordIndex = str.lastIndexOf(lastWorld);
        String updateFirsHalfStr = str.substring(0, lastWordIndex);
        String updateSecondHalfStr = str.substring(lastWordIndex + lengthLastElement, str.length());
        String updateStr = updateFirsHalfStr.concat(updateSecondHalfStr);
        System.out.println(updateStr);
        return updateStr;
    }
}

