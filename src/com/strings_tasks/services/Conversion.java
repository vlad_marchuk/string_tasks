package com.strings_tasks.services;

public class Conversion {

    public String convertIntToString(int num) {
        String numStr = String.valueOf(num);
        return numStr;
    }

    public String convertDoubleToString(double num) {
        String numStr = String.valueOf(num);
        return numStr;
    }

    public int convertStringToInt(String numStr) {
        int numInt = Integer.parseInt (numStr);
        return numInt;
    }
    public double convertStringToDouble(String numStr) {
        double numInt =  Double.parseDouble(numStr);
        return numInt;
    }

}

