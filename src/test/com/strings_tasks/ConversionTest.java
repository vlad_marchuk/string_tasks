package test.com.strings_tasks;

import com.strings_tasks.services.Conversion;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

class ConversionTest {

    Conversion cut = new Conversion();

    static Arguments[] convertIntToStringTestArgs() {
        return new Arguments[]{
                Arguments.arguments(50, "50"),
                Arguments.arguments(12, "12"),
                Arguments.arguments(1, "1")
        };
    }
        static Arguments[] convertDoubleToStringTestArgs() {
            return new Arguments[] {
                    Arguments.arguments(12.12,"12.12"),
                    Arguments.arguments(0.236,"0.236")
            };
    }
    static Arguments[] convertStringToIntTestTestArgs() {
        return new Arguments[] {
                Arguments.arguments("15",15),
                Arguments.arguments("1256",1256),
        };
    }
    static Arguments[] convertStringToDoubleTestArgs() {
        return new Arguments[] {
                Arguments.arguments("15.125",15.125),
                Arguments.arguments("0.12365",0.12365),
        };
    }



    @ParameterizedTest
    @MethodSource("convertIntToStringTestArgs")
    void convertIntToStringTest(int num, String expected) {
        Assertions.assertEquals(expected, cut.convertIntToString(num));
    }


    @ParameterizedTest
    @MethodSource("convertDoubleToStringTestArgs")
    void convertDoubleToStringTest(double num, String expected) {
        Assertions.assertEquals(expected, cut.convertDoubleToString(num));
    }

    @ParameterizedTest
    @MethodSource("convertStringToIntTestTestArgs")
    void convertStringToIntTest(String num, int expected) {
        Assertions.assertEquals(expected, cut.convertStringToInt(num));
    }
    @ParameterizedTest
    @MethodSource("convertStringToDoubleTestArgs")
    void convertStringToDoubleTest(String num, double expected) {
        Assertions.assertEquals(expected, cut.convertStringToDouble(num));
    }


}