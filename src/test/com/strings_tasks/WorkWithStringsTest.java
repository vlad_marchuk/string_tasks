package test.com.strings_tasks;

import com.strings_tasks.services.WorkWithStrings;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;

import java.util.Scanner;

class WorkWithStringsTest {

    private final Scanner scanner = Mockito.mock(Scanner.class);
    WorkWithStrings cut = new WorkWithStrings(scanner);



    static Arguments[] getLenghtMinWordTestArgs() {
        return new Arguments[] {
                Arguments.arguments("Трактористы ехали на ранчо, но забыли трактора и копали лопатами?!",1),
                Arguments.arguments("Погода хорошая!",6),

        };
    }
    static Arguments[] addSpacesAfterPunctuationMarksTestArgs() {
        return new Arguments[] {
                Arguments.arguments("Трактористы,ехали на. ранчо, но забыли. трактора,и копали;лопатами","Трактористы, ехали на. ранчо, но забыли. трактора, и копали; лопатами"),
                Arguments.arguments("Погода,хорошая,очень!","Погода, хорошая, очень! "),

        };
    }
    static Arguments[] leaveOneCharacterInTheStringTestArgs() {
        return new Arguments[] {
                Arguments.arguments("Длинна слова для операции должна быть больше или ровна 3","Длин а с ов  д я  пер ц      ж   быть     ш            3"),
                Arguments.arguments("Погода,хорошая,очень!","Пог да,х р ш я  чень!"),

        };
    }
    static Arguments[] removePartFromStringTestArgs() {
        return new Arguments[] {
                Arguments.arguments("Привет пострижися",10,10,"Указанные параметры обрезания строки выходят за границу даной строки "),
                Arguments.arguments("Привет пострижися",6,3,"Приветстрижися"),
                Arguments.arguments("Привет пострижися",2,3,"Прт пострижися"),

        };
    }
    static Arguments[] flipTheLineTestArgs() {
        return new Arguments[] {
                Arguments.arguments("Молоко,ехали на. ранчо, но забыли. трактора,и копали;лопатами","иматапол;илапок и,ароткарт .илыбаз он ,очнар .ан илахе,околоМ"),
                Arguments.arguments("Garret","terraG"),

        };
    }
    static Arguments[] deleteLastWordTestArgs() {
        return new Arguments[] {
                Arguments.arguments("Молоко,ехали на. ранчо, но забыли. трактора,и копали; лопатами !","Молоко,ехали на. ранчо, но забыли. трактора,и копали;  !"),
                Arguments.arguments("Привет, давай пока !","Привет, давай  !"),

        };
    }

    static Arguments[] countWordsInTheLineTestArgs() {
        return new Arguments[] {
                Arguments.arguments("Привет, давай пока !",3),
                Arguments.arguments("Привет, пока !",2),


        };
    }

    @ParameterizedTest
    @MethodSource("getLenghtMinWordTestArgs")
    void getLenghtMinWordTest(String str, int expected) {
        Assertions.assertEquals(expected, cut.getLenghtMinWord(str));
    }

    @ParameterizedTest
    @MethodSource("addSpacesAfterPunctuationMarksTestArgs")
    void addSpacesAfterPunctuationMarksTest(String updateString, String expected) {
        Assertions.assertEquals(expected, cut.addSpacesAfterPunctuationMarks(updateString));
    }
    @ParameterizedTest
    @MethodSource("leaveOneCharacterInTheStringTestArgs")
    void leaveOneCharacterInTheStringTest(String updateString, String expected) {
        Assertions.assertEquals(expected, cut.leaveOneCharacterInTheString(updateString));
    }
    @ParameterizedTest
    @MethodSource("countWordsInTheLineTestArgs")
    void countWordsInTheLineTest(String str, int expected) {
        Mockito.when(scanner.nextLine()).thenReturn(str);
        int actual = cut.countWordsInTheLine();
        Assertions.assertEquals(expected, actual);
    }
    @ParameterizedTest
    @MethodSource("removePartFromStringTestArgs")
    void removePartFromStringTest(String str, int startCut, int lengthCut, String expected) {
        Assertions.assertEquals(expected, cut.removePartFromString(str,startCut,lengthCut));
    }
    @ParameterizedTest
    @MethodSource("flipTheLineTestArgs")
    void flipTheLineTest(String str, String expected) {
        Assertions.assertEquals(expected, cut.flipTheLine(str));
    }
    @ParameterizedTest
    @MethodSource("deleteLastWordTestArgs")
    void deleteLastWordTest(String str, String expected) {
        Assertions.assertEquals(expected, cut.deleteLastWord(str));
    }
}